# SchoolFinder application #

The SchoolFinder application was created as part of a group project for CIS 550 at the University of Pennsylvania to demonstrate the use of databases for web applications. While the databases are not currently active, the datasets used and the code used to get and clean them are provided.

## Webapp ##

The web application can be found under the webapp directory, and includes both the node server and the AngularJS front end code. See the README under the webapp directory for more information