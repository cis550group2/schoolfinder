DELIMITER $$

DROP TRIGGER IF EXISTS `schoolfinder`.`gradechange_update` $$
DROP TRIGGER IF EXISTS `schoolfinder`.`gradechange_insert` $$

CREATE
  TRIGGER `gradechange_update` AFTER UPDATE
  ON `grades`
  FOR EACH ROW BEGIN
    SET @total_students=(
      SELECT
        st.asian_males + st.asian_females +
        st.hispanic_males + st.hispanic_females + 
        st.black_males + st.black_females + 
        st.white_males + st.white_females + 
        st.am_indian_males + st.am_indian_females + 
        st.mult_races_males + st.mult_races_females +
        st.hawaiian_pacific_males + st.hawaiian_pacific_females
      FROM school_totals st
      WHERE st.agency_nces_id=NEW.agency_nces_id AND st.nces_id=NEW.nces_id
    );
    SET @div_students=(
      SELECT
        st.asian_males + st.asian_females +
        st.hispanic_males + st.hispanic_females + 
        st.black_males + st.black_females + 
        st.am_indian_males + st.am_indian_females + 
        st.mult_races_males + st.mult_races_females +
        st.hawaiian_pacific_males + st.hawaiian_pacific_females
      FROM school_totals st
      WHERE st.agency_nces_id=NEW.agency_nces_id AND st.nces_id=NEW.nces_id
    );
    UPDATE schools s
    SET s.total_students=@total_students, s.div_students=100 * @div_students/@total_students
    WHERE s.agency_nces_id=NEW.agency_nces_id AND s.nces_id=NEW.nces_id;
  END$$
  
CREATE
  TRIGGER `gradechange_insert` AFTER INSERT
  ON `grades`
  FOR EACH ROW BEGIN
    SET @total_students=(
      SELECT
        st.asian_males + st.asian_females +
        st.hispanic_males + st.hispanic_females + 
        st.black_males + st.black_females + 
        st.white_males + st.white_females + 
        st.am_indian_males + st.am_indian_females + 
        st.mult_races_males + st.mult_races_females +
        st.hawaiian_pacific_males + st.hawaiian_pacific_females
      FROM school_totals st
      WHERE st.agency_nces_id=NEW.agency_nces_id AND st.nces_id=NEW.nces_id
    );
    SET @div_students=(
      SELECT
        st.asian_males + st.asian_females +
        st.hispanic_males + st.hispanic_females + 
        st.black_males + st.black_females + 
        st.am_indian_males + st.am_indian_females + 
        st.mult_races_males + st.mult_races_females +
        st.hawaiian_pacific_males + st.hawaiian_pacific_females
      FROM school_totals st
      WHERE st.agency_nces_id=NEW.agency_nces_id AND st.nces_id=NEW.nces_id
    );
    UPDATE schools s
    SET s.total_students=@total_students, s.div_students=100 * @div_students/@total_students
    WHERE s.agency_nces_id=NEW.agency_nces_id AND s.nces_id=NEW.nces_id;
  END$$
  
DELIMITER ;