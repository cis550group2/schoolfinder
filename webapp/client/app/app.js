angular.module('TeacherApp', [
  'ui.bootstrap',
  'ui.router',
  'ui.grid',
  'ngCookies',
  'uiGmapgoogle-maps',
  'ngMap'
  ])
.constant("Grades", [
  // Permit no constraint.
  {
    code: undefined,
    name: ""
  },{
    code:"PK",
    name: "Pre K"
  }, {
    code: "KG",
    name: "Kindergarten"
  }, {
    code: "01",
    name: "1st"
  }, {
    code: "02",
    name: "2nd"
  }, {
    code: "03",
    name: "3rd"
  }, {
    code: "04",
    name: "4th"
  }, {
    code: "05",
    name: "5th"
  }, {
    code: "06",
    name: "6th"
  }, {
    code: "07",
    name: "7th"
  }, {
    code: "08",
    name: "8th"
  }, {
    code: "09",
    name: "9th"
  }, {
    code: "10",
    name: "10th"
  }, {
    code: "11",
    name: "11th"
  }, {
    code: "12",
    name: "12th"
  }])
.constant("MinMax", [
  "Min",
  "Max"
])
.constant("UrbanCentricTypes", [
  {
    code: 11,
    name: "City, Large"
  }, {
    code: 12,
    name: "City, Midsize"
  }, {
    code: 13,
    name: "City, Small"
  }, {
    code: 21,
    name: "Suburb, Large"
  }, {
    code: 22,
    name: "Suburb, Midsize"
  }, {
    code: 23,
    name: "Suburb, Small"
  }, {
    code: 31,
    name: "Town, Fringe"
  }, {
    code: 32,
    name: "Town, Distant"
  }, {
    code: 33,
    name: "Town, Remote"
  }, {
    code: 41,
    name: "Rural, Fringe"
  }, {
    code: 42,
    name: "Rural, Distant"
  }, {
    code: 43,
    name: "Rural, Remote"
  }
])
.constant('AvailableCols', [
//  { name: 'school_name', display: 'School Name' },
  { name: 'location_city', display: 'City' },
  { name: 'location_state', display: 'State' },
  { name: 'agency_name', display: 'Education Agency Name' },
  { name: 'charter', display: 'Charter School'},
  { name: 'magnet', display: 'Magnet School'},
  { name: 'title_1_eligible', display: 'School Title 1 Eligible'},
  { name: 'urban_centric_code', display: 'Urban Centric Code (raw)'},
  { name: 'low_grade', display: 'Lowest Grade'},
  { name: 'high_grade', display: 'Highest Grade'},
  { name: 'diversity', display: 'Percent Minority Students'},
//  { name: 'location_lat', display: 'Location Latitude'},
//  { name: 'location_long', display: 'Location Longitude'},
  { name: 'satreading', display: 'Avg SAT Reading, Zip Code'},
  { name: 'satwriting', display: 'Avg SAT Writing, Zip Code'},
  { name: 'satmath', display: 'Avg SAT Math, Zip Code'},
  { name: 'act', display: 'Avg ACT, Zip Code'},
  { name: 'livingIndex', display: 'Living Index, Zip Code'},
  { name: 'avgIncome', display: 'Avg Income, Zip Code'},
  { name: 'avgHouseValue', display: 'Avg House Value, Zip Code'},
  { name: 'salary', display: 'State Avg Teacher Salary'}
])
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider.state({
    'abstract': true,
    name: 'schoolsearch',
    url: '/schoolsearch',
    templateUrl: 'app/routes/schoolsearch.html',
    controller: 'schoolsearchCtrl',
    authenticate: true,
    resolve: {
      lastQuery: function($q, $rootScope, userService, MinMax) {
        var deferred = $q.defer();
        userService.getLastQuery($rootScope.globals.loggedUser.username).then(
          function(result){
            if (result) {
              deferred.resolve(result);
            } else {
              var defaultObject = {
                "loc_types": [],
                "highorder": "Min",
                "loworder": "Min",
                "fields": ["location_city", "location_state"]
              };
              deferred.resolve(defaultObject);
            }
          },
          function(err){
            console.log("Error resolving last query");
            var defaultObject = {
                "loc_types": [],
                "highorder": "Min",
                "loworder": "Min",
                "fields": ["location_city", "location_state"]
              };
            deferred.resolve(defaultObject);
          }
        );
        return deferred.promise;
      },
      firstResults: function(lastQuery, dbService) {
        return dbService.search(lastQuery, lastQuery.fields, 0, 50);
      }
    }
  })
  .state({
    name: 'schoolsearch.list',
    url: '/list',
    templateUrl: 'app/routes/schoolsearch.list.html',
  })
  .state({
    name: 'schoolsearch.map',
    url: '/map',
    templateUrl: 'app/routes/schoolsearch.map.html'
  })
  .state({
    name: 'login',
    url: '/login',
    templateUrl: 'app/routes/login.html',
    controller: 'loginCtrl'
  })
  .state({
    name: 'register',
    url: '/register',
    templateUrl: 'app/routes/register.html',
    controller: 'registerCtrl'
  }).state({
    name: 'school',
    url: '/school/:agency/:id',
    templateUrl: 'app/routes/school.html',
    controller: 'schoolCtrl',
    resolve: {
      schoolData: function($stateParams, dbService) {
        return dbService.getSchool($stateParams.agency, $stateParams.id);
      }
    }
  });
  $urlRouterProvider.when("/schoolsearch", "/schoolsearch/list");
  $urlRouterProvider.otherwise("/login");
})
.run(function($rootScope, $state, $cookies, $http) {

  $rootScope.globals = $cookies.getObject('globals') || {};
  if ($rootScope.globals.currentUser) {
      $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
  }

  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
      var loggedIn = $rootScope.globals.loggedUser;

      // If user isn’t authenticated, redirect to login
      if (toState.authenticate && !loggedIn){
        $state.transitionTo("login");
        event.preventDefault(); 
      }

      // If already logged in, redirect to school search
      if (toState.name === 'login' && loggedIn){
        $state.go("schoolsearch.list");
        event.preventDefault();
      };

    });
});