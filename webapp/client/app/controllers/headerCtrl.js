'use strict'
angular.module('TeacherApp').controller('headerCtrl',
function($scope, $rootScope, $state, authService) {
  $scope.logout = function(){
    authService.logout();
    $state.go('login');
  };
  $scope.isLoggedIn = function() {
    return $rootScope.globals.loggedUser;
  };
});