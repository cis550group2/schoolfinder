angular.module('TeacherApp')
.service('userService', function($http, $q){
  var registerUrl = '/register';
  var lastqueryUrl = '/lastquery';
  this.register = function (username, password){
    query = {};
    query.username = username;
    query.password = password;
    var deferred = $q.defer();
    $http({
      method: 'POST',
      url: registerUrl,
      params: query
    }).then(function successCallback(response) {
      deferred.resolve(response.data);
    }, function errorCallback(response) {
      console.log("Error", response);
      deferred.reject(response);
    });
    return deferred.promise;
  };

  this.saveLastQuery = function(username, lastquery) {
    query = {};
    query.username = username;
    //query.query = lastquery;
    var deferred = $q.defer();
    $http.post(lastqueryUrl, lastquery, {params: query}).then(function successCallback(response) {
      deferred.resolve(response.data);
    }, function errorCallback(response) {
      console.log("Error", response);
      deferred.reject(response);
    });
    return deferred.promise;
  };

  this.getLastQuery = function(username) {
    query = {};
    query.username = username;
    var deferred = $q.defer();
    $http({
      method: 'GET',
      url: lastqueryUrl,
      params: query
    }).then(function successCallback(response) {
      deferred.resolve(response.data);
    }, function errorCallback(response) {
      console.log("Error", response);
      deferred.reject(response);
    });
    return deferred.promise;
  };
});