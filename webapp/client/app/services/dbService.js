angular.module('TeacherApp')
.service('dbService', function($http, $q){
  var endpointUrl = "/search";
  this.search = function (query, fields, offset, limit){
    // console.log(query);
    query['fields'] = fields.slice();
    query['fields'].push('agency_nces_id');
    query['fields'].push('school_nces_id');
    query['fields'].push('school_name');
    query['fields'].push('location_lat');
    query['fields'].push('location_long');
    query['offset'] = offset;
    query['limit'] = limit;
    var deferred = $q.defer();
    $http({
      method: 'GET',
      url: endpointUrl,
      params: query
    }).then(function successCallback(response) {
      deferred.resolve(response.data);
    }, function errorCallback(response) {
      console.log("Error", response);
      deferred.reject(response);
    });
    return deferred.promise;
  };
  this.getSchool = function (agency_id, school_id) {
    var deferred = $q.defer();
    $http({
      method: 'GET',
      url: "/schooldata/" + agency_id + "/" + school_id
    }).then(function successCallback(response) {
      deferred.resolve(response.data);
    }, function errorCallback(response) {
      console.log('Error', response);
      deferred.reject(response);
    });
    return deferred.promise;
  };
});