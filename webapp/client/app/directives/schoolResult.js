angular.module('TeacherApp')
.controller("schoolResultCtrl", function ($scope) {
  
})
.directive("schoolResult", function () {
  return {
    restrict: 'E',
    scope: {
      result: '='
    },
    controller: 'schoolResultCtrl',
    template: "<div>{{result.name}}</div>"
  }
});