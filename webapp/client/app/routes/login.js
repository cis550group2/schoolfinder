angular.module('TeacherApp').controller('loginCtrl',
function ($scope, $state, authService) {

	$scope.username = "";
	$scope.password = "";
	$scope.invalid = false;

	$scope.logIn = function() {
		authService.login($scope.username, $scope.password).then(function(success) {
			if (success) {
				authService.setCookies($scope.username, $scope.password);
				$state.go("schoolsearch.list");
			} else {
				$scope.invalid = true;
				$scope.resetForm();
			}
 		});
	}

	$scope.resetForm = function() {
		$scope.username = "";
		$scope.password = "";
	}
});