angular.module('TeacherApp').controller('registerCtrl',
function ($scope, $location, userService) {

	$scope.newUsername = "";
	$scope.newPassword = "";
	$scope.invalid = false;

	$scope.register = function() {
		userService.register($scope.newUsername, $scope.newPassword).then(function(success) {
			if (success) {
				// TODO: Indicate success popup? then after timeout redirect to login
				$location.path("/login")
			} else {
				$scope.invalid = true;
				$scope.resetForm();
			}
		});
	}

	$scope.resetForm = function() {
		$scope.newUsername = "";
		$scope.newPassword = "";
	}
});