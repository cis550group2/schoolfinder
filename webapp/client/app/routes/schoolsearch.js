angular.module('TeacherApp').controller('schoolsearchCtrl',
function ($scope, $location, $rootScope, dbService, authService, userService, Grades, MinMax, UrbanCentricTypes, AvailableCols, uiGmapGoogleMapApi, lastQuery, firstResults) {
  var uiGridApi;

    $scope.pageSize = 50;
    $scope.grades = Grades;
    $scope.availableCols = AvailableCols;
    $scope.minmax = MinMax;
    $scope.loc_types = UrbanCentricTypes;
    $scope.sort_opts = [
    {
      display: 'Min Academic Performance',
      value: 'minacademicperformance',
      tooltip: 'Find schools with the lowest academic performance using our proprietary measure'
    },
    {
      display: 'Max Academic Performance',
      value: 'maxacademicperformance',
      tooltip: 'Find schools with the highest academic performance using our proprietary measure'
    },
    {
      display: 'Salary Power',
      value: 'salarypower',
      tooltip: 'Sort by maximizing our proprietary measure of salary power using salary and cost of living estimates'
    }
    ];
    
    $scope.fields = ['location_city', 'location_state'];

    $scope.updateGridColVisibility = function() {
      $scope.gridOptions.columnDefs.forEach(function(e) {
        e.visible = e.field == 'school_name' || (-1 != $scope.fields.indexOf(e.field));
      });
      uiGridApi.core.refresh();
    }
    var columns = AvailableCols.map(function(e) {
      var result = {
        field: e.name,
        displayName: e.display,
        width: '20%',
        cellTooltip: true,
        enableSorting: false
      };
      switch(e.name){
        case 'salary':
        case 'avgIncome':
        case 'avgHouseValue':
          result['cellTemplate'] = '<div class="ui-grid-cell-contents" >${{grid.getCellValue(row, col)}}</div>';
        default:
          break;
      }
      return result;
    });
    // Schoool name is required, and so is not selectable.
    columns.unshift({
      field: 'school_name',
      displayName: 'School Name',
      width: '20%',
      cellTooltip: true,
      enableSorting: false,
      cellTemplate: '<div class="ui-grid-cell-contents" ><a href="" ui-sref="school({agency: {{row.entity.agency_nces_id}}, id: {{row.entity.school_nces_id}}})">{{grid.getCellValue(row, col)}}</a></div>'
    });
    $scope.gridOptions = {
      data: [],
      columnDefs: columns,
      onRegisterApi: function(gridApi){ uiGridApi = gridApi;}
    };
    
    $scope.toggle_list = function (code, list) {
      var index = list.indexOf(code);
      if (index > -1) {
        list.splice(index, 1);
      } else {
        list.push(code);
      }
    };
    

    $scope.search = function(offset) {
      dbService.search($scope.query, $scope.fields, offset, $scope.pageSize)
      .then(function (result) {
        $scope.gridOptions.data = result;
        $scope.translateResultsToMapMarkers();
      });
      
      userService.saveLastQuery($rootScope.globals.loggedUser.username, $scope.query)
      .then(function (result) {
        console.log("success saving query");
      });

    }

    $scope.clearSearch = function() {
      $scope.results = [];
    }

    $scope.logout = function() {
      authService.logout();
      $location.path("/login");    
    }

    $scope.map = { center: { latitude: 40.1451, longitude: -99.6680 }, zoom: 4 };

    $scope.markers = [];

    $scope.templateUrl = "";

    $scope.clickedMarker = function(marker, event, model) {
      model.show = !model.show;
    }

    $scope.translateResultsToMapMarkers = function() {
      $scope.markers = [];
      $scope.map = { center: { latitude: 40.1451, longitude: -99.6680 }, zoom: 4 };
      for (var i = 0; i < $scope.gridOptions.data.length; i++) {
        var school = $scope.gridOptions.data[i];
        var newMarker = {
          id: i,
          latitude: school.location_lat,
          longitude: school.location_long,
          school: school,
          show: false,
          title: school.school_name
        }
        $scope.markers.push(newMarker);
      }
    }

    $scope.query = lastQuery;
    $scope.fields = lastQuery.fields;
    delete lastQuery.fields; // To avoid having it in two places
    $scope.gridOptions.data = firstResults;
    $scope.translateResultsToMapMarkers();

});