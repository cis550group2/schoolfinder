angular.module('TeacherApp').controller('schoolCtrl',
function ($scope,
  // Defined in app.js
  UrbanCentricTypes,
  // Injected from router
  schoolData) {
  $scope.data = schoolData;
  $scope.transform_urbancode = function (code) {
    return UrbanCentricTypes[UrbanCentricTypes.map(function(e){
      return e.code;
    }).indexOf(code)].name;
  };
  $scope.transform_phone = function (phone) {
    var areaCode = Math.floor(phone / 10000000);
    var three = Math.floor((phone - (10000000 * areaCode))/10000);
    return "(" + areaCode + ") " + three + "-" + (phone % 10000);
  }
});