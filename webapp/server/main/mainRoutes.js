var mainController = require('./mainController.js');
var mysql      = require('mysql');
var util       = require('util');
var mongo = require('mongodb').MongoClient;
var bcrypt = require('bcrypt');

var mongoEndpoint = 'mongodb://54.236.63.82/user';

var mysqlConfig = {
  host     : 'cis550-us-east-1b.csl09i9dpiaa.us-east-1.rds.amazonaws.com',
  user     : 'readonly',
  password : 'iwanttoread',
  database : 'schoolfinder',
  port     : 3306
};

var grades = ["PK", "KG", "01", "02", "03", "04", "05",
      "06", "07", "08", "09", "10", "11", "12"];

var dataSQLColMap = {
  'agency_nces_id': 's.agency_nces_id',
  'agency_name': 'ea.agency_name',
  'school_nces_id': 's.nces_id',
  'school_name': 's.school_name',
  'location_city': 's.location_city',
  'location_state': 's.location_state',
  'charter': 's.charter',
  'magnet': 's.magnet',
  'title_1_eligible': 's.title_1_eligible',
  'urban_centric_code': 's.urban_centric_code',
  'low_grade': 's.low_grade',
  'high_grade': 's.high_grade',
  'fte_teachers': 's.fte_teachers',
  'diversity': 's.div_percent',
  'total_students': 's.total_students',
  'location_lat': 's.location_lat',
  'location_long': 's.location_long',
  'satreading': 'zc.satreading',
  'satwriting': 'zc.satwriting',
  'satmath': 'zc.satmath',
  'act': 'zc.act',
  'academic_index': 'ap.index',
  'livingIndex': 'zc.livingIndex',
  'avgIncome': 'zc.avgIncome',
  'avgHouseValue': 'zc.avgHouseValue',
  'salary' : 'st.salary2013'
};

var school_cols = ["low_grade", "high_grade", "location_street", "location_city", "location_state", "location_zip", "location_zip4", dataSQLColMap['location_lat'],dataSQLColMap['location_long'], "mailing_street", "mailing_city", "mailing_state", "mailing_zip", "mailing_zip4", "phone_number", "urban_centric_code", "fte_teachers", "total_students", "div_percent", "free_lunch_eligible", "reduced_lunch_eligible", "magnet", "charter", "shared_time", "title_1_eligible", "school_wide_title_1", "satreading", "satmath", "satwriting", "act", "avgIncome", dataSQLColMap['salary'], "avgHouseValue"];

var tableJoin = "((((ed_agencies ea JOIN schools s ON ea.agency_nces_id=s.agency_nces_id) JOIN zipcodes zc ON s.location_zip=zc.zipCode) LEFT JOIN states st ON st.state_code=s.location_state) JOIN act_performance ap ON ap.code=s.location_state)";

function extractToList(input) {
  // No input case
  if (!input) {
    return [];
  }
  // One input case
  if (!Array.isArray(input)) {
    return [input];
  }
  // All other cases.
  return input;
}

function extractOrdering(query) {
  switch(query.ordering) {
    case "salarypower":
      // Normalized cost of living index. Lower value means more desirable since dollar goes farther.
      living_index = util.format("(100/IFNULL(%s,100))", dataSQLColMap['livingIndex']);
      // Ratio of local average salary to average teacher salary. Higher value means teachers do better. If either is missing use 1.
      salary_ratio = util.format("(IFNULL(%s/%s, 1))", dataSQLColMap['salary'], dataSQLColMap['avgIncome']);
      // Inverse of years of average take home salary necessary for a 10% down payment on an average house. Higher value means teachers do better.
      inv_down_payment_salary_years = util.format("(%s /(%s/10))", dataSQLColMap['salary'], dataSQLColMap['avgHouseValue']);
      result = util.format("(%s*%s*%s) DESC", living_index, salary_ratio, inv_down_payment_salary_years);
      return result;
    case "minacademicperformance":
      return util.format("(%s) ASC", dataSQLColMap['academic_index']);
    case "maxacademicperformance":
      return util.format("(%s) DESC", dataSQLColMap['academic_index']);
    default:
      return ""; // Nothing
  }
}

function extractConditionList(query) {
  var result = [];
  var tmp;
  // loc types
  tmp = extractToList(query.loc_types);
  if (tmp.length) {
    result.push(util.format("(%s IN (%s))", dataSQLColMap['urban_centric_code'], tmp.join(',')));
  }
  if(query.lowgrade) {
    if(query.loworder) {
      if (query.loworder == "Max") {
        tmp = grades.slice(0, grades.indexOf(query.lowgrade) + 1);
      } else {
        tmp = grades.slice(grades.indexOf(query.lowgrade));
      }
      result.push(util.format("(%s IN (%s))", dataSQLColMap['low_grade'], tmp.map(function(a){return '\'' + a + '\'';}).join(',')));
    } else {
      result.push(util.format("(%s = %s)", dataSQLColMap['low_grade'], query.lowgrade.code));
    }
  }
  if(query.highgrade) {
    if(query.highorder) {
      if (query.highorder == "Max") {
        tmp = grades.slice(0, grades.indexOf(query.highgrade) + 1);
      } else {
        tmp = grades.slice(grades.indexOf(query.highgrade));
      }
      result.push(util.format("(%s IN (%s))", dataSQLColMap['high_grade'], tmp.map(function(a){return '\'' + a + '\'';}).join(',')));
    } else {
      result.push(util.format("(%s = %s)", dataSQLColMap['high_grade'], query.highgrade.code));
    }
  }
  if (query.magnet) {
    result.push("(s.magnet = 1)");
  }

  if (query.charter) {
    result.push("(s.charter = 1)");
  }

  if (query.title1) {
    result.push("(s.title_1_eligible = 1)");
  }

  // Diversity
  if (query.diversityMin && !isNaN(query.diversityMin)) {
    result.push(util.format("(%s >= %d)", dataSQLColMap['diversity'], query.diversityMin));
  }
  if (query.diversityMax && !isNaN(query.diversityMax)) {
    result.push(util.format("(%s <= %d)", dataSQLColMap['diversity'], query.diversityMax));
  }
  
  // Student teacher ratio
  if (query.stRatioMin && !isNaN(query.stRatioMin)) {
    result.push(util.format("(%s/%s >= %d)", dataSQLColMap['total_students'], dataSQLColMap['fte_teachers'], query.stRatioMin));
  }
  if (query.stRatioMax && !isNaN(query.stRatioMax)) {
    result.push(util.format("(%s/%s <= %d)", dataSQLColMap['total_students'], dataSQLColMap['fte_teachers'], query.stRatioMax));
  }

  // SAT/ACT
  if (query.satWriteMin && !isNaN(query.satWriteMin)) {
    result.push(util.format("(%s >= %d)", dataSQLColMap['satwriting'], query.satWriteMin));
  }
  if (query.satWriteMax && !isNaN(query.satWriteMax)) {
    result.push(util.format("(%s <= %d)", dataSQLColMap['satwriting'], query.satWriteMax));
  }
  if (query.satReadMin && !isNaN(query.satReadMin)) {
    result.push(util.format("(%s >= %d)", dataSQLColMap['satreading'], query.satReadMin));
  }
  if (query.satReadMax && !isNaN(query.satReadMax)) {
    result.push(util.format("(%s <= %d)", dataSQLColMap['satreading'], query.satReadMax));
  }
  if (query.satMathMin && !isNaN(query.satMathMin)) {
    result.push(util.format("(%s >= %d)", dataSQLColMap['satmath'], query.satMathMin));
  }
  if (query.satMathMax && !isNaN(query.satMathMax)) {
    result.push(util.format("(%s <= %d)", dataSQLColMap['satmath'], query.satMathMax));
  }
  if (query.actMin && !isNaN(query.actMin)) {
    result.push(util.format("(%s >= %d)", dataSQLColMap['act'], query.actMin));
  }
  if (query.actMax && !isNaN(query.actMax)) {
    result.push(util.format("(%s <= %d)", dataSQLColMap['act'], query.actMax));
  }

  return result;
}

module.exports = function (app) {
  // app === mainRouter injected from middlware.js

  app.get('/', mainController.homepage);
  app.get('/search', function(req,res,next){
    var offset = Number.isInteger(req.query.offset) ? req.query.offset : 0;
    var limit = Number.isInteger(req.query.limit) ? req.query.limit : 50;
    var ret_columns = extractToList(req.query.fields).map(function(a) {
      return dataSQLColMap[a] + ' ' + a;
    });
    var conditions =  extractConditionList(req.query);
    var ordering = extractOrdering(req.query);

    // Build query.
    var query = 'SELECT ';
    query += (ret_columns.length) ? ret_columns.join(',') : '*';
    // Need to left join states so that schools not in the 50 states get nulls for salary
    query += ' FROM ' 
    query += tableJoin;
    if(conditions.length) {
      query += ' WHERE ' + conditions.join(' AND ');
    }
    if(ordering) {
        query += ' ORDER BY ' + ordering;
    }
    query += ' LIMIT ' + offset + ',' + limit +';';

    // Send query.
    var connection = mysql.createConnection(mysqlConfig);
    connection.connect();
    connection.query(
      query,
      function(err, rows, fields) {
        if(err) return next(err);
        res.send(rows);
      }
    );
    connection.end();
  });
  app.get('/schooldata/:agency/:school', function(req, res, next) {
    var query = util.format("SELECT %s FROM %s WHERE %s=%s AND %s=%s;", school_cols.join(","), tableJoin, dataSQLColMap['agency_nces_id'], req.params.agency, dataSQLColMap['school_nces_id'], req.params.school);
    var connection = mysql.createConnection(mysqlConfig);
    connection.connect();
    connection.query(
      query,
      function(err, rows, fields) {
        if(err) return next(err);
        res.send(rows[0]);
      }
    );
    connection.end();
  });
  app.post('/login', function(req,res,next) {
    // Connect to the db
    mongo.connect(mongoEndpoint, function(err, db) {
      if(!err) {
          var collection = db.collection('user');
          var newUser = {'username':req.query.username};
          collection.findOne(newUser, function(err, result) {
            if (!err) {
              // Match was found, compare the passwords
              if (result) {
                var realPassword = result.password;
                var passwordToCheck = req.query.password;
                bcrypt.compare(passwordToCheck, realPassword).then(function(doCredentialsMatch) {
                  res.json(doCredentialsMatch);
                });

              } else {
                console.log("Error invalid username or pwd");
                res.json(false);
              }

            // Unexpected error when querying db
            } else {
              res.status(401).send();
            }
          })
        // Unexpected error connecting to db
        } else {
          res.json(false);
        }
    });
  });

  app.post('/register', function(req,res,next) {
    const saltRounds = 10;
    bcrypt.genSalt(saltRounds, function(err, salt) {
      bcrypt.hash(req.query.password, salt, function(err, hash) {
        mongo.connect(mongoEndpoint, function(err, db) {
          if(!err) {
            var collection = db.collection('user');
            var newUser = {'username':req.query.username, 'password': hash, 'salt': salt};
            collection.insert(newUser, {w:1}, function(err, result) {
              if (err) {
                console.log("Error registering new user");
                res.json(false);
              } else {
                console.log("User was registered successfully");
                res.json(true);
              }
            })
          } else {
            res.json(false);
          }
        });
      });
    });
  });

  app.post('/lastquery', function(req,res,next) {
    mongo.connect(mongoEndpoint, function(err, db) {
      if(!err) {
          var collection = db.collection('user');
          var newUser = {'username':req.query.username};
          collection.findOne(newUser, function(err, result) {
            if (!err) {
              // Match was found, compare the passwords
              if (result) {
                result.lastquery = req.body;
                collection.update({'username':req.query.username}, result, function(err, result) {
                  if (err) {
                    console.log("Error updating last query");
                  } else {
                    console.log("Success saving query");
                  }
                });

              } else {
                console.log("Error saving last query");
                res.json(false);
              }

            // Unexpected error when querying db
            } else {
              res.status(401).send();
            }
          })
        // Unexpected error connecting to db
        } else {
          res.json(false);
        }
    });
  });

  app.get('/lastquery', function(req,res,next) {
    mongo.connect(mongoEndpoint, function(err, db) {
      if(!err) {
          var collection = db.collection('user');
          var newUser = {'username':req.query.username};
          collection.findOne(newUser, function(err, result) {
            if (!err) {
              // Match was found, compare the passwords
              if (result) {
                console.log("Success getting last query");
                res.json(result.lastquery);
              } else {
                console.log("Error getting last query");
                res.json(false);
              }

            // Unexpected error when querying db
            } else {
              res.status(401).send();
            }
          })
        // Unexpected error connecting to db
        } else {
          res.json(false);
        }
    });
  });
};
