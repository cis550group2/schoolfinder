import csv
import argparse
import time
import mysql.connector

# Note: Added IGNORE to prevent errors when agency already exists
agency_insert = """INSERT IGNORE INTO Ed_Agencies (\
    agency_nces_id,
    agency_state_id,
    agency_name
) VALUES (%s,%s,%s);"""

school_insert = """INSERT INTO Schools (\
    agency_nces_id,\
    nces_id,\
    school_name,\
    school_state_id,\
    nces_type_code,\
    low_grade,\
    high_grade,\
    phone_number,\
    mailing_street,\
    mailing_city,\
    mailing_state,\
    mailing_zip,\
    mailing_zip4,\
    location_street,\
    location_city,\
    location_state,\
    location_zip,\
    location_zip4,\
    location_lat,\
    location_long,\
    county_name,\
    county_code,\
    urban_centric_code,\
    race_cat_7,\
    fte_teachers,\
    nces_status_code,\
    free_lunch_eligible,\
    reduced_lunch_eligible,\
    congressional_district,\
    title_1_eligible,\
    school_wide_title_1,\
    magnet,\
    charter,\
    shared_time,\
    bie_school\
) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"""

grade_insert = """INSERT INTO Grades (\
    agency_nces_id,\
    nces_id,\
    grade_level,\
    asian_male,\
    asian_female,\
    hispanic_male,\
    hispanic_female,\
    black_male,\
    black_female,\
    white_male,\
    white_female,\
    am_indian_male,\
    am_indian_female,\
    mult_races_male,\
    mult_races_female,\
    hawaiian_pacific_male,\
    hawaiian_pacific_female
) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"""

parser = argparse.ArgumentParser(description="Parse School CCD data to SQL")
parser.add_argument('filepath', type=str)
parser.add_argument('-sqlhost', default='localhost', type=str)
parser.add_argument('-sqluser', type=str, required=True)
parser.add_argument('-sqlpwd', type=str, required=True)
parser.add_argument('-sqldb', default='schoolfinder', type=str)
args = parser.parse_args()

def add_demographic_data(record, row, start_index, tag):
    grade = dict()
    grade['level'] = tag
    if int(row[start_index]) not in (0, -2):
        grade['total'] = int(row[start_index])
        grade['am_indian_male'] = int(row[start_index + 1])
        grade['am_indian_female'] = int(row[start_index + 2])
        grade['asian_male'] = int(row[start_index + 3])
        grade['asian_female'] = int(row[start_index + 4])
        grade['hispanic_male'] = int(row[start_index + 5])
        grade['hispanic_female'] = int(row[start_index + 6])
        grade['black_male'] = int(row[start_index + 7])
        grade['black_female'] = int(row[start_index + 8])
        grade['white_male'] = int(row[start_index + 9])
        grade['white_female'] = int(row[start_index + 10])
        if int(row[start_index + 11]) is not -2:
            grade['hawaiian_pacific_male'] = int(row[start_index + 11])
        if int(row[start_index + 12]) is not -2:
            grade['hawaiian_pacific_female'] = int(row[start_index + 12])
        if int(row[start_index + 13]) is not -2:
            grade['2plus_races_male'] = int(row[start_index + 13])
        if int(row[start_index + 14]) is not -2:
            grade['2plus_races_female'] = int(row[start_index + 14])
        record['students'].append(grade)

print("Populating schools")
start_time = time.time()
with open(args.filepath, 'rb') as csv_file:
    # Connect to the database
    connection = mysql.connector.connect(host=args.sqlhost, user=args.sqluser, passwd=args.sqlpwd, db=args.sqldb)
    cursor = connection.cursor()

    csv_reader = csv.reader(csv_file)
    # Throw away the two header rows
    next(csv_reader)
    next(csv_reader)
    count = 0
    for row in csv_reader:
        count += 1
        record = dict()
        record['ansi_state_code'] = int(row[1])  # FIPST
        record['nces_id'] = int(row[3])  # SCHNO
        record['state_id'] = str(row[5])  # SEASCH09
        record['name'] = row[7]  # SCHNAM09

        # Educational agency
        record['ed_agency'] = {}
        record['ed_agency']['nces_id'] = int(row[2])  # LEAID
        record['ed_agency']['state_id'] = str(row[4])  # STID09
        record['ed_agency']['name'] = str(row[6])  # LEANM09

        # Contact information
        record['contact'] = dict()
        if row[8] not in ('M', 'N'):
            record['contact']['phone'] = row[8]  # PHONE09 as string
        record['contact']['street'] = str(row[9])
        record['contact']['city'] = str(row[10])
        record['contact']['state'] = str(row[11])
        if row[12] != 'M':
            record['contact']['zip'] = int(row[12])
        if row[13] not in ('M', '', 'NULL'):
            record['contact']['zip4'] = int(row[13])

        # Physical Location
        record['location'] = dict()
        record['location']['street'] = str(row[14])
        record['location']['city'] = str(row[15])
        record['location']['state'] = str(row[16])
        if row[17] != 'M':
            record['location']['zip'] = int(row[17])
        if row[18] not in ('M', '', 'NULL'):
            record['location']['zip4'] = int(row[18])
        record['location']['latlong'] = {
            'x': float(row[22]),
            'y': float(row[23])
        }

        # NCES Type Code
        # 1=Regular school
        # 2=Special education school
        # 3=Vocational school
        # 4=Other/alternative school
        # 5=Reportable program
        record['nces_type_code'] = int(row[19])  # TYPE09

        # 1=operational
        # 2=closed since last report
        # 3=opened since last report (new)
        # 4=operational since last report but not reported
        # 5=changed education agency, still operational
        # 6=temporarily closed
        # 7=will be opened within 2 years
        # 8=previously closed but has reopened this year
        record['nces_status_code'] = int(row[20])  # TYPE09

        record['county'] = dict()
        if row[25] not in ('N', 'M'):
            record['county']['name'] = row[25]  # CONAME09
        if row[24] not in ('N', 'M'):
            record['county']['state_code'] = int(int(row[24]) / 1000)
            record['county']['county_code'] = int(row[24]) % 1000
        if row[26] not in ('N', 'M'):
            record['congressional_district'] = int(row[26])  # CDCODE09
        record['fte_teachers'] = float(row[27])
        record['features'] = dict()

        # Two digits.
        # 11=large city
        # 12=mid city
        # 12=small city
        # 21=large suburb
        # 22=mid suburb
        # 23=small suburb
        # 31=fringe town
        # 32=distant town
        # 33=remote town
        # 41=fringe rural
        # 42=distant rural
        # 43=remote rural
        if row[21] not in ('N', 'M'):
            record['features']['nces_urban-centric_code'] = int(row[21])  # ULOCAL09

        if row[28] is not 'N':
            record['features']['grades'] = dict()
            record['features']['grades']['low'] = row[28]
            record['features']['grades']['hi'] = row[29]
        if row[31] not in ('N', 'M'):
            record['features']['titleI_eligible'] = (1 == int(row[31]))
        if row[32] not in ('N', 'M'):
            record['features']['sch_wide_titleI_eligible'] = (1 == int(row[32]))
        if row[33] not in ('N', 'M'):
            record['features']['magnet'] = (1 == int(row[33]))
        if row[34] not in ('N', 'M'):
            record['features']['charter'] = (1 == int(row[34]))
        if row[35] not in ('N', 'M', ''):
            record['features']['shared_time'] = (1 == int(row[35]))
        # Bureau of Indian Education school
        record['features']['BIE_school'] = (1 == int(row[36]))

        if int(row[37]) not in (-1, -2):
            record['free_lunch_eligible'] = int(row[37])
        if int(row[38]) not in (-1, -2):
            record['reduced_lunch_eligible'] = int(row[38])
        if int(row[39]) not in (-1, -2):
            record['total_lunch_eligible'] = int(row[39])

        record['racecat_7'] = int(row[40]) == 7  # RACECAT09

        record['students'] = list()
        add_demographic_data(record, row, 41, 'PK')
        add_demographic_data(record, row, 56, 'KG')
        add_demographic_data(record, row, 71, '01')
        add_demographic_data(record, row, 86, '02')
        add_demographic_data(record, row, 101, '03')
        add_demographic_data(record, row, 116, '04')
        add_demographic_data(record, row, 131, '05')
        add_demographic_data(record, row, 146, '06')
        add_demographic_data(record, row, 161, '07')
        add_demographic_data(record, row, 176, '08')
        add_demographic_data(record, row, 191, '09')
        add_demographic_data(record, row, 206, '10')
        add_demographic_data(record, row, 221, '11')
        add_demographic_data(record, row, 236, '12')
        add_demographic_data(record, row, 251, 'UG')

        # Insert!
        cursor.execute(agency_insert, (
            record['ed_agency']['nces_id'],
            record['ed_agency']['state_id'],
            record['ed_agency']['name']
        ))
        cursor.execute(school_insert, (
            record['ed_agency']['nces_id'],
            record['nces_id'],
            record['name'],
            record['state_id'],
            record['nces_type_code'],
            record['features'].get('grades', dict()).get('low', None),
            record['features'].get('grades', dict()).get('hi', None),
            record['contact'].get('phone', None),
            record['contact']['street'],
            record['contact']['city'],
            record['contact']['state'],
            record['contact'].get('zip', None),
            record['contact'].get('zip4', None),
            record['location']['street'],
            record['location']['city'],
            record['location']['state'],
            record['location'].get('zip', None),
            record['location'].get('zip4', None),
            record['location']['latlong']['x'],
            record['location']['latlong']['y'],
            record['county'].get('name', None),
            record['county'].get('county_code', None),
            record['features'].get('nces_urban-centric_code', None),
            record['racecat_7'],
            record['fte_teachers'],
            record['nces_status_code'],
            record.get('free_lunch_eligible', None),
            record.get('reduced_lunch_eligible', None),
            record.get('congressional_district', None),
            record['features'].get('titleI_eligible', None),
            record['features'].get('sch_wide_titleI_eligible', None),
            record['features'].get('magnet', None),
            record['features'].get('charter', None),
            record['features'].get('shared_time', None),
            record['features'].get('BIE_school', None)
        ))
        for grade in record['students']:
            cursor.execute(grade_insert, (
                record['ed_agency']['nces_id'],
                record['nces_id'],
                grade['level'],
                grade['asian_male'],
                grade['asian_female'],
                grade['hispanic_male'],
                grade['hispanic_female'],
                grade['black_male'],
                grade['black_female'],
                grade['white_male'],
                grade['white_female'],
                grade['am_indian_male'],
                grade['am_indian_female'],
                grade.get('mult_races_male', None),
                grade.get('mult_races_female', None),
                grade.get('hawaiian_pacific_male', None),
                grade.get('hawaiian_pacific_female', None)
            ))
        if count % 500 is 0:
            print("Completed {} records, {} seconds elapsed.".format(count, time.time() - start_time))

    connection.commit()
    connection.close()
    print("Upload complete in {} seconds".format(time.time() - start_time))
