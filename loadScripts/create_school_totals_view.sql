CREATE VIEW school_totals AS (
	SELECT agency_nces_id, nces_id,
		SUM(IFNULL(asian_male,0)) asian_males,
		SUM(IFNULL(asian_female,0)) asian_females,
		SUM(IFNULL(hispanic_male,0)) hispanic_males,
		SUM(IFNULL(hispanic_female,0)) hispanic_females,
		SUM(IFNULL(black_male,0)) black_males,
		SUM(IFNULL(black_female,0)) black_females,
		SUM(IFNULL(white_male,0)) white_males,
		SUM(IFNULL(white_female,0)) white_females,
		SUM(IFNULL(am_indian_male,0)) am_indian_males,
		SUM(IFNULL(am_indian_female,0)) am_indian_females,
		SUM(IFNULL(mult_races_male,0)) mult_races_males,
		SUM(IFNULL(mult_races_female,0)) mult_races_females,
		SUM(IFNULL(hawaiian_pacific_male,0)) hawaiian_pacific_males,
		SUM(IFNULL(hawaiian_pacific_female,0)) hawaiian_pacific_females
	FROM grades
    GROUP BY agency_nces_id, nces_id
)
