CREATE TABLE ed_agencies(
	agency_nces_id VARCHAR(50),
    agency_state_id VARCHAR(50) NOT NULL, # How the state refers to it
	agency_name VARCHAR(100) NOT NULL,
    PRIMARY KEY(agency_nces_id)
);

CREATE TABLE schools(
	agency_nces_id VARCHAR(50),
    nces_id VARCHAR(50),
    school_name VARCHAR(100) NOT NULL,
    school_state_id VARCHAR(50) NOT NULL, # How the state refers to it
    nces_type_code INTEGER NOT NULL,
    # Grade information
    low_grade CHARACTER(2),
    high_grade CHARACTER(2),
    # Mailing location
    phone_number CHARACTER(10),
    mailing_street VARCHAR(50),
    mailing_city VARCHAR(50),
    mailing_state CHARACTER(2),
    mailing_zip INTEGER(5),
    mailing_zip4 INTEGER(4),
    # Physical location
    location_street VARCHAR(50),
    location_city VARCHAR(50),
    location_state CHARACTER(2),
    location_zip INTEGER(5),
    location_zip4 INTEGER(4),
    location_lat FLOAT,
    location_long FLOAT,
    # County
    county_name VARCHAR(50),
    county_code INTEGER,
    urban_centric_code INTEGER,
    race_cat_7 Boolean NOT NULL, # True for 7, False for 5. Determines where to look for grade info.
    # features
    fte_teachers FLOAT NOT NULL,
    nces_status_code INTEGER NOT NULL,
    free_lunch_eligible INTEGER, # Some are missing
    reduced_lunch_eligible INTEGER, # Some are missing
    congressional_district INTEGER, # Some are missing
    # All of these may be NULL, indicating missing or N/A
    title_1_eligible BOOLEAN,
    school_wide_title_1 BOOLEAN,
    magnet BOOLEAN,
    charter BOOLEAN,
    shared_time BOOLEAN,
    bie_school BOOLEAN,
    
    PRIMARY KEY(agency_nces_id, nces_id),
    CONSTRAINT fk_agency FOREIGN KEY(agency_nces_id) REFERENCES ed_agencies(agency_nces_id),
    #CONSTRAINT fk_zip_code FOREIGN KEY(location_zip) REFERENCES zip_codes(zipcode),
    # 'UG' means ungraded. 'NA' means no students reported
    CONSTRAINT grade_vals CHECK (low_grade AND high_grade IN ('UG','NA','KG','PK','01','02','03','04','05','06','07','08','09','10','11','12')),
    CONSTRAINT status_valid CHECK (nces_status_code BETWEEN 1 AND 8),
    CONSTRAINT nces_type_valid CHECK (nces_type_code BETWEEN 1 AND 5),
    CONSTRAINT urban_code_valid CHECK (urban_centric_code IN (11,12,13,21,22,23,31,32,33,41,42,43))
);

# 7 category grades
CREATE TABLE grades(
	agency_nces_id VARCHAR(50),
    nces_id VARCHAR(50),
    grade_level CHARACTER(2),
    asian_male INTEGER NOT NULL,
    asian_female INTEGER NOT NULL,
    hispanic_male INTEGER NOT NULL,
    hispanic_female INTEGER NOT NULL,
    black_male INTEGER NOT NULL,
    black_female INTEGER NOT NULL,
    white_male INTEGER NOT NULL,
    white_female INTEGER NOT NULL,
    am_indian_male INTEGER NOT NULL,
    am_indian_female INTEGER NOT NULL,
    # Next 4 are NULL if race_cat_7 is True
    mult_races_male INTEGER,
    mult_races_female INTEGER,
    hawaiian_pacific_male INTEGER,
    hawaiian_pacific_female INTEGER,
    PRIMARY KEY(agency_nces_id, nces_id, grade_level),
    CONSTRAINT fk_students FOREIGN KEY (agency_nces_id, nces_id) REFERENCES schools(agency_nces_id, nces_id) ON DELETE CASCADE,
    CONSTRAINT grade_level_vals CHECK (grade_level IN ('UG','KG','PK','01','02','03','04','05','06','07','08','09','10','11','12'))
);