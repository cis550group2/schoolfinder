import csv
import argparse
from pymongo import MongoClient
from pymongo import ASCENDING

parser = argparse.ArgumentParser(description="Parse School CCD data")
parser.add_argument('filepath', type=str)
args = parser.parse_args()


def add_demographic_data(record, row, start_index, tag):
    if int(row[start_index]) is 0:
        record['students'][tag] = 0
    elif int(row[start_index]) is not -2:
        record['students'][tag] = dict()
        record['students'][tag]['total'] = int(row[start_index])
        record['students'][tag]['am_indian_male'] = int(row[start_index + 1])
        record['students'][tag]['am_indian_female'] = int(row[start_index + 2])
        record['students'][tag]['asian_male'] = int(row[start_index + 3])
        record['students'][tag]['asian_female'] = int(row[start_index + 4])
        record['students'][tag]['hispanic_male'] = int(row[start_index + 5])
        record['students'][tag]['hispanic_female'] = int(row[start_index + 6])
        record['students'][tag]['black_male'] = int(row[start_index + 7])
        record['students'][tag]['black_female'] = int(row[start_index + 8])
        record['students'][tag]['white_male'] = int(row[start_index + 9])
        record['students'][tag]['white_female'] = int(row[start_index + 10])
        if int(row[start_index + 11]) is not -2:
            record['students'][tag]['hawaiian_pacific_male'] = int(row[start_index + 11])
        if int(row[start_index + 12]) is not -2:
            record['students'][tag]['hawaiian_pacific_female'] = int(row[start_index + 12])
        if int(row[start_index + 13]) is not -2:
            record['students'][tag]['2plus_races_male'] = int(row[start_index + 13])
        if int(row[start_index + 14]) is not -2:
            record['students'][tag]['2plus_races_female'] = int(row[start_index + 14])

# Connect to the database and create a new collection
db = MongoClient().schools
collection = db.schools
collection.drop()
collection.create_index([
    ('ed_agency.nces_id', ASCENDING),
    ('nces_id', ASCENDING)
], unique=True, name='pk')
batch = []
batchSize = 400
print("Populating the 'schools' collection")
with open(args.filepath, 'rb') as csv_file:
    csv_reader = csv.reader(csv_file)
    # Throw away the two header rows
    next(csv_reader)
    next(csv_reader)
    count = 0
    for row in csv_reader:
        record = dict()
        record['ansi_state_code'] = int(row[1])  # FIPST
        record['nces_id'] = int(row[3])  # SCHNO
        record['state_id'] = str(row[5])  # SEASCH09
        record['name'] = row[7]  # SCHNAM09

        # Educational agency
        record['ed_agency'] = {}
        record['ed_agency']['nces_id'] = int(row[2])  # LEAID
        record['ed_agency']['state_id'] = str(row[4])  # STID09
        record['ed_agency']['name'] = str(row[6])  # LEANM09

        # Contact information
        record['contact'] = dict()
        if row[8] not in ('M', 'N'):
            record['contact']['phone'] = int(row[8])  # PHONE09
        record['contact']['street'] = str(row[9])
        record['contact']['city'] = str(row[10])
        record['contact']['state'] = str(row[11])
        if row[12] != 'M':
            record['contact']['zip'] = '{:0>5}'.format(row[12])
        if row[13] not in ('M', ''):
            record['contact']['zip4'] = '{:0>4}'.format(row[13])

        # Physical Location
        record['location'] = dict()
        record['location']['street'] = str(row[14])
        record['location']['city'] = str(row[15])
        record['location']['state'] = str(row[16])
        if row[17] != 'M':
            record['location']['zip'] = '{:0>5}'.format(row[17])
        if row[18] not in ('M', ''):
            record['location']['zip4'] = '{:0>4}'.format(row[18])
        record['location']['latlong'] = {
            'x': float(row[22]),
            'y': float(row[23])
        }

        # NCES Type Code
        # 1=Regular school
        # 2=Special education school
        # 3=Vocational school
        # 4=Other/alternative school
        # 5=Reportable program
        record['nces_type_code'] = int(row[19])  # TYPE09

        # 1=operational
        # 2=closed since last report
        # 3=opened since last report (new)
        # 4=operational since last report but not reported
        # 5=changed education agency, still operational
        # 6=temporarily closed
        # 7=will be opened within 2 years
        # 8=previously closed but has reopened this year
        record['nces_status_code'] = int(row[20])  # TYPE09

        record['county'] = dict()
        if row[25] not in ('N', 'M'):
            record['county']['name'] = row[25]  # CONAME09
        if row[24] not in ('N', 'M'):
            record['county']['state_code'] = int(int(row[24]) / 1000)
            record['county']['county_code'] = int(row[24]) % 1000
        if row[26] not in ('N', 'M'):
            record['congressional_district'] = int(row[26])  # CDCODE09
        record['fte_teachers'] = float(row[27])
        record['features'] = dict()

        # Two digits.
        # 11=large city
        # 12=mid city
        # 12=small city
        # 21=large suburb
        # 22=mid suburb
        # 23=small suburb
        # 31=fringe town
        # 32=distant town
        # 33=remote town
        # 41=fringe rural
        # 42=distant rural
        # 43=remote rural
        if row[21] not in ('N', 'M'):
            record['features']['nces_urban-centric_code'] = int(row[21])  # ULOCAL09

        if row[28] is 'UG':
            record['features']['grades'] = row[28]
        elif row[28] is not 'N':
            record['features']['grades'] = dict()
            record['features']['grades']['low'] = row[28]
            record['features']['grades']['hi'] = row[29]
        if row[31] not in ('N', 'M'):
            record['features']['titleI_eligible'] = (1 == int(row[31]))
        if row[32] not in ('N', 'M'):
            record['features']['sch_wide_titleI_eligible'] = (1 == int(row[32]))
        if row[33] not in ('N', 'M'):
            record['features']['magnet'] = (1 == int(row[33]))
        if row[34] not in ('N', 'M'):
            record['features']['charter'] = (1 == int(row[34]))
        if row[35] not in ('N', 'M', ''):
            record['features']['shared_time'] = (1 == int(row[35]))
        # Bureau of Indian Education school
        record['features']['BIE_school'] = (1 == int(row[36]))

        record['students'] = dict()
        if int(row[37]) not in (-1, -2):
            record['students']['free_lunch_eligible'] = int(row[37])
        if int(row[38]) not in (-1, -2):
            record['students']['reduced_lunch_eligible'] = int(row[38])
        if int(row[39]) not in (-1, -2):
            record['students']['total_lunch_eligible'] = int(row[39])

        record['students']['racecat'] = int(row[40])  # RACECAT09
        add_demographic_data(record, row, 41, 'pk')
        add_demographic_data(record, row, 56, 'kg')
        add_demographic_data(record, row, 71, '1')
        add_demographic_data(record, row, 86, '2')
        add_demographic_data(record, row, 101, '3')
        add_demographic_data(record, row, 116, '4')
        add_demographic_data(record, row, 131, '5')
        add_demographic_data(record, row, 146, '6')
        add_demographic_data(record, row, 161, '7')
        add_demographic_data(record, row, 176, '8')
        add_demographic_data(record, row, 191, '9')
        add_demographic_data(record, row, 206, '10')
        add_demographic_data(record, row, 221, '11')
        add_demographic_data(record, row, 236, '12')
        add_demographic_data(record, row, 251, 'ungraded')
        if int(row[266]) not in (-1, -2):
            record['students']['total'] = int(row[266])

        # Completed record construction
        batch.append(record)
        count += 1
        if len(batch) == batchSize:
            print("Completed {} records".format(count))
            collection.insert_many(batch)
            batch = []
    if len(batch) is not 0:
        print("Completed {} records".format(count))
        collection.insert_many(batch)
    print("Upload complete")
