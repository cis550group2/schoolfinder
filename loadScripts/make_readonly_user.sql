DROP USER 'readonly'@'%';
CREATE USER 'readonly'@'%' IDENTIFIED BY 'iwanttoread';

GRANT SELECT ON schoolfinder.* TO 'readonly'@'%';

FLUSH PRIVILEGES;